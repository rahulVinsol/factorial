enum FactorialError: Error {
	case invalidInput
	case overflow
}

func factorial(n: Int) throws -> Int {
	switch n { 
		case let n where n > 20: throw FactorialError.overflow
		case let n where n < 0: throw FactorialError.invalidInput
		case let n where n == 0: return 1
		default: return try n == 1 ? 1 : n * factorial(n: n-1)  
	}
}

try! factorial(n:21)
